package com.mytaxi.android_demo;


import android.support.annotation.StringRes;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.jakewharton.espresso.OkHttp3IdlingResource;
import com.mytaxi.android_demo.activities.AuthenticationActivity;
import com.mytaxi.android_demo.utils.storage.SharedPrefStorage;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import javax.inject.Inject;

import okhttp3.OkHttpClient;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestActivityTest {
    String username, password;
    String username1, password1;

    private OkHttpClient client = new OkHttpClient();
    IdlingResource idlingResource = OkHttp3IdlingResource.create(
            "okhttp", client);

    @Inject
    SharedPrefStorage mSharedPrefStorage;

    @Rule
    public ActivityTestRule<AuthenticationActivity> mActivityRule = new ActivityTestRule<>(
            AuthenticationActivity.class);

    @Before
    public void initLoginCredentials() {
        // Specify a valid string
        username = "crazydog335";
        password = "venture";
        username1 = "don";
        password1 = "password";
    }


    @Before
    public void setUp() {
        IdlingRegistry.getInstance().register(idlingResource);
    }
    @After
    public void tearDown() {
        IdlingRegistry.getInstance().unregister(idlingResource);
    }


    @Test
    public void test6ValidLoginSuccess() throws InterruptedException {
        // Type text and then press the button.
        onView(withId(R.id.edt_username)).perform(typeText(username));
        onView(withId(R.id.edt_password)).perform(typeText(password));
        onView(withId(R.id.btn_login)).perform(click());

         //Check that the textSearch field is present.
        onView(withId(R.id.textSearch))
                .check(matches(isDisplayed()));


    }

    @Test
    public void test5LoginWithNoPassword() throws InterruptedException {
                // Type text and then press the button.
                        onView(withId(R.id.edt_username)).perform(typeText(username), closeSoftKeyboard());
                onView(withId(R.id.edt_password)).perform(clearText());
              onView(withId(R.id.btn_login)).perform(click(longClick()));

                       checkSnackBarDisplayedByMessage(R.string.message_login_fail);
    }

    private void checkSnackBarDisplayedByMessage(@StringRes int message) {
        onView(withText(message))
                .check(matches(withEffectiveVisibility(
                        ViewMatchers.Visibility.VISIBLE
                )));
    }


}